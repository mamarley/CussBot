<?php
require_once("config.php");

$responseArray=array();
$responseArray["status"]="success";
$responseArray["message"]=array();

try{
	$dbh=new PDO("pgsql:dbname=$sqlDb",$sqlUsername,$sqlPassword);
}catch(Exception $e){
	$responseArray["status"]="error";
	array_push($responseArray["message"],"Failed to connect to the database.");
}

if(isset($dbh)){
	$channelConditionArgs=array();
	$channelConditionPlaceholders=array();
	if(isset($_POST["filterChannels"])&&is_array($_POST["filterChannels"])){
		foreach($_POST["filterChannels"] as $filterChannel){
			array_push($channelConditionPlaceholders,"?");
			array_push($channelConditionArgs,$filterChannel);
		}
	}
	if(count($channelConditionPlaceholders)>0){
		$channelConditionString="channel IN (" . implode(",",$channelConditionPlaceholders) . ")";
	}else{
		$channelConditionString="TRUE";
	}

	$nickConditionArgs=array();
	$nickConditionPlaceholders=array();
	if(isset($_POST["filterNicks"])&&is_array($_POST["filterNicks"])){
		foreach($_POST["filterNicks"] as $filterNick){
			array_push($nickConditionPlaceholders,"?");
			array_push($nickConditionArgs,$filterNick);
		}
	}
	if(count($nickConditionPlaceholders)>0){
		$nickConditionString="nick IN (" . implode(",",$nickConditionPlaceholders) . ")";
	}else{
		$nickConditionString="TRUE";
	}

	if(isset($_POST["channels"])){
		try{
			$stmt=$dbh->prepare("SELECT channel FROM channels ORDER BY channel ASC");
			$stmt->execute();
			$responseArray["channels"]=$stmt->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			$responseArray["status"]="error";
			array_push($responseArray["message"],"Failed to fetch the channel list.");
		}
	}

	if(isset($_POST["channels"])||isset($_POST["nicks"])){
		try{
			if(count($channelConditionArgs)>0){
				$stmt=$dbh->prepare("SELECT DISTINCT nick FROM nicks JOIN uses ON nicks.nickid=uses.nickid JOIN channels ON uses.channelid=channels.channelid WHERE $channelConditionString ORDER BY nick ASC");
			}else{
				$stmt=$dbh->prepare("SELECT nick FROM nicks ORDER BY nick ASC");
			}
			$i=0;
			foreach($channelConditionArgs as $channelConditionArg){
				$stmt->bindValue(++$i,$channelConditionArg);
			}
			$stmt->execute();
			$responseArray["nicks"]=$stmt->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			$responseArray["status"]="error";
			array_push($responseArray["message"],"Failed to fetch the nick list.");
		}
	}

	if(isset($_POST["channels"])||isset($_POST["nicks"])||isset($_POST["graphData"])){
		try{
			$stmt=$dbh->prepare("SELECT fullname FROM categories");
			$stmt->execute();
			$categories=$stmt->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			$responseArray["status"]="error";
			array_push($responseArray["message"],"Failed to fetch the category list.");
		}
		
		try{
			$stmt=$dbh->prepare("SELECT fullname,COUNT(fullname) AS count FROM uses JOIN words ON uses.wordid=words.wordid JOIN categories ON words.categoryid=categories.categoryid " . (count($nickConditionArgs)>0?"JOIN nicks ON uses.nickid=nicks.nickid ":"") . (count($channelConditionArgs)>0?"JOIN channels ON uses.channelid=channels.channelid ":"") . "WHERE $channelConditionString AND $nickConditionString GROUP BY fullname");
			$i=0;
			foreach($channelConditionArgs as $channelConditionArg){
				$stmt->bindValue(++$i,$channelConditionArg);
			}
			foreach($nickConditionArgs as $nickConditionArg){
				$stmt->bindValue(++$i,$nickConditionArg);
			}
			$stmt->execute();
			$categoryCounts=$stmt->fetchAll(PDO::FETCH_ASSOC);
		}catch(Exception $e){
			$responseArray["status"]="error";
			array_push($responseArray["message"],"Failed to fetch the category count data.");
		}
		
		if(isset($categories)&&isset($categoryCounts)){
			$responseArray["lineData"]=array();
			$responseArray["pieData"]=array();
			$responseArray["lineTicks"]=array();
			
			$pieData=array();
			foreach($categories as $category){
				$pieData[$category["fullname"]]=0;
			}
			foreach($categoryCounts as $categoryCount){
				$pieData[$categoryCount["fullname"]]=$categoryCount["count"];
			}
			foreach($pieData as $key=>$value){
				array_push($responseArray["pieData"],array("label"=>$key . "s","data"=>$value));
			}
			
			foreach($categories as $category){
				try{
					$stmt=$dbh->prepare("SELECT to_char(date,'YYYYMMDD') AS day,COUNT(date) AS numwords FROM uses JOIN words ON uses.wordid=words.wordid JOIN categories ON words.categoryid=categories.categoryid " . (count($nickConditionArgs)>0?"JOIN nicks ON uses.nickid=nicks.nickid ":"") . (count($channelConditionArgs)>0?"JOIN channels ON uses.channelid=channels.channelid ":"") . "WHERE $channelConditionString AND $nickConditionString AND date>(now()-interval '9 days') AND fullname=? GROUP BY day;");
					$i=0;
					foreach($channelConditionArgs as $channelConditionArg){
						$stmt->bindValue(++$i,$channelConditionArg);
					}
					foreach($nickConditionArgs as $nickConditionArg){
						$stmt->bindValue(++$i,$nickConditionArg);
					}
					$stmt->bindValue(++$i,$category["fullname"]);
					$stmt->execute();
					$categoryHistories=$stmt->fetchAll(PDO::FETCH_ASSOC);
				}catch(Exception $e){
					$responseArray["status"]="error";
					array_push($responseArray["message"],"Failed to fetch the category history for category \"$category\".");
				}
				
				$currentDate=date_create(date("Y-m-d"));
				$dataPoints=array();
				$dataPoints[date_format($currentDate,"Ymd")]=0;
				for($i=0;$i<9;$i++){
					$dataPoints[date_format(date_sub($currentDate,date_interval_create_from_date_string("1 days")),"Ymd")]=0;
				}
				foreach($categoryHistories as $categoryHistory){
					$dataPoints[$categoryHistory["day"]]=$categoryHistory["numwords"];
				}
				
				$j=0;
				$flotDataPoints=array();
				foreach($dataPoints as $dataPoint){
					array_push($flotDataPoints,array($j,$dataPoint));
					$j--;
				}
				array_push($responseArray["lineData"],array("label"=>$category["fullname"] . "s","data"=>$flotDataPoints));
			}
			
			$j=0;
			foreach($dataPoints as $date=>$dataPoint){
				array_push($responseArray["lineTicks"],array($j,$date));
				$j--;
			}
		}
	}
}

header("Content-Type: application/json");
print(json_encode($responseArray));
?>
