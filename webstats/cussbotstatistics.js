var channelCountSpan;
var channelCountPluralSpan;
var nickCountSpan;
var nickCountPluralSpan;
var cusswordCountSpan;
var cusswordCountPluralSpan;
var categoryCountSpan;
var categoryCountPluralSpan;
var channelListviewSelect;
var nickListviewSelect;

var totalChannelCount;
var totalNickCount;

var channelOptions;
var nickOptions;

function onLoad(){
	if(window.XMLHttpRequest){
		xmlHttpRequest=new XMLHttpRequest();
	}else{
		asyncAlert("Browser does not support XMLHttpRequest!");
		throw new Error("Browser does not support XMLHttpRequest!");
	}
	
	channelCountSpan=document.getElementById("channelCount");
	channelCountPluralSpan=document.getElementById("channelCountPlural");
	nickCountSpan=document.getElementById("nickCount");
	nickCountPluralSpan=document.getElementById("nickCountPlural");
	cusswordCountSpan=document.getElementById("cusswordCount");
	cusswordCountPluralSpan=document.getElementById("cusswordCountPlural");
	categoryCountSpan=document.getElementById("categoryCount");
	categoryCountPluralSpan=document.getElementById("categoryCountPlural");
	channelListviewSelect=document.getElementById("channelListview");
	nickListviewSelect=document.getElementById("nickListview");
	
	totalChannelCount=0;
	totalChannelCount=0;
	
	xmlHttpRequest.onreadystatechange=function(){
		if(xmlHttpRequest.readyState==4){
			if(xmlHttpRequest.status==200){
				var data=JSON.parse(xmlHttpRequest.responseText);
				
				if(data.status=="error"){
					asyncAlert(data.message);
				}
				
				if(data.channels!=null){
					totalChannelCount=data.channels.length;
					updateChannelSummary(0);
					channelOptions=new Array();
					var channelOptionsFrag=document.createDocumentFragment();
					data.channels.forEach(function(channel){
						var channelOption=document.createElement("option");
						channelOption.appendChild(document.createTextNode(channel.channel));
						channelOption.value=encodeURIComponent(channel.channel);
						channelOptions.push(channelOption);
						channelOptionsFrag.appendChild(channelOption);
					});
					replaceChildren(channelListviewSelect,channelOptionsFrag);
				}
				
				if(data.nicks!=null){
					totalNickCount=data.nicks.length;
					updateNickSummary(0);
					nickOptions=new Array();
					var nickOptionsFrag=document.createDocumentFragment();
					data.nicks.forEach(function(nick){
						var nickOption=document.createElement("option");
						nickOption.appendChild(document.createTextNode(nick.nick));
						nickOption.value=encodeURIComponent(nick.nick);
						nickOptions.push(nickOption);
						nickOptionsFrag.appendChild(nickOption);
					});
					replaceChildren(nickListviewSelect,nickOptionsFrag);
				}
				
				if(data.pieData!=null){
					var cusswordCount=0;
					var categoryCount=0;
					data.pieData.forEach(function(pieDatum){
						cusswordCount+=pieDatum.data;
						if(pieDatum.data>0){
							categoryCount++;
						}
					});
					replaceChildren(cusswordCountSpan,document.createTextNode(cusswordCount));
					replaceChildren(cusswordCountPluralSpan,document.createTextNode(cusswordCount==1?"":"s"));
					replaceChildren(categoryCountSpan,document.createTextNode(categoryCount));
					replaceChildren(categoryCountPluralSpan,document.createTextNode(categoryCount==1?"y":"ies"));
					
					$.plot($("#pieGraph"),data.pieData,{
						series:{
							pie:{ 
								show:true,
								radius:1,
								label:{
									show:true,
									radius:1,
									formatter:function(label,series){
										return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'+label+'<br/>'+series.data[0][1]+' ('+Math.round(series.percent)+'%)</div>';
									},
									background:{opacity:0.8}
								}
							}
						},
						legend:{
							show:false
						}
					});
				}
				
				if(data.lineData!=null&&data.lineTicks!=null){
					$.plot("#lineGraph",data.lineData,{
						lines:{
							show:true
						},
						points:{
							show:true
						},
						xaxis:{
							ticks:data.lineTicks
						},
						legend:{		 
							container:$("#lineGraphLegend"),
							noColumns:0
						}
					});
				}
			}else if(xmlHttpRequest.status!=0){
				asyncAlert("Request failed with status \""+xmlHttpRequest.status+" "+xmlHttpRequest.statusText+"\"");
			}
		}
	};
	
	updateChannels();
}

function updateChannels(){
	xmlHttpRequest.open("POST","data.php",true);
	xmlHttpRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	xmlHttpRequest.send("channels=true");
}

function updateNicks(){
	xmlHttpRequest.open("POST","data.php",true);
	xmlHttpRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	
	var selectedChannelCount=0;
	var filterChannels="";
	channelOptions.forEach(function(channelOption){
		if(channelOption.selected){
			selectedChannelCount++;
			filterChannels+="&filterChannels[]="+channelOption.value;
		}
	});
	
	xmlHttpRequest.send("nicks=true"+filterChannels);
	updateChannelSummary(selectedChannelCount);
}

function updateGraphs(){	
	xmlHttpRequest.open("POST","data.php",true);
	xmlHttpRequest.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	
	var selectedChannelCount=0;
	var filterChannels="";
	channelOptions.forEach(function(channelOption){
		if(channelOption.selected){
			selectedChannelCount++;
			filterChannels+="&filterChannels[]="+channelOption.value;
		}
	});
	
	var selectedNickCount=0;
	var filterNicks="";
	nickOptions.forEach(function(nickOption){
		if(nickOption.selected){
			selectedNickCount++;
			filterNicks+="&filterNicks[]="+nickOption.value;
		}
	});
	
	xmlHttpRequest.send("graphData=true"+filterChannels+filterNicks);
	updateChannelAndNickSummary(selectedChannelCount,selectedNickCount);
}

function updateChannelAndNickSummary(selectedChannelCount,selectedNickCount){
	updateChannelSummary(selectedChannelCount);
	updateNickSummary(selectedNickCount);
}

function updateChannelSummary(selectedChannelCount){
	if(selectedChannelCount>0){
		replaceChildren(channelCountSpan,document.createTextNode(selectedChannelCount));
		replaceChildren(channelCountPluralSpan,document.createTextNode(selectedChannelCount==1?"":"s"));
	}else{
		replaceChildren(channelCountSpan,document.createTextNode(totalChannelCount));
		replaceChildren(channelCountPluralSpan,document.createTextNode(totalChannelCount==1?"":"s"));
	}
}

function updateNickSummary(selectedNickCount){
	if(selectedNickCount>0){
		replaceChildren(nickCountSpan,document.createTextNode(selectedNickCount));
		replaceChildren(nickCountPluralSpan,document.createTextNode(selectedNickCount==1?"":"s"));
	}else{
		replaceChildren(nickCountSpan,document.createTextNode(totalNickCount));
		replaceChildren(nickCountPluralSpan,document.createTextNode(totalNickCount==1?"":"s"));
	}
}

function replaceChildren(node,children){
	removeChildren(node);
	node.appendChild(children);
}

function removeChildren(node){
	while(node.hasChildNodes()){
		node.removeChild(node.lastChild);
	}
}

function asyncAlert(text){
	setTimeout(function(){alert(text);},0);
}
