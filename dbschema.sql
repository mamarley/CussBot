--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: categories; Type: TABLE; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE TABLE categories (
    categoryid bigint NOT NULL,
    shortname character varying(20) NOT NULL,
    fullname character varying(255) NOT NULL
);


ALTER TABLE categories OWNER TO cussbot;

--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: public; Owner: cussbot
--

CREATE SEQUENCE categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE categories_id_seq OWNER TO cussbot;

--
-- Name: categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cussbot
--

ALTER SEQUENCE categories_id_seq OWNED BY categories.categoryid;


SET default_with_oids = true;

--
-- Name: channels; Type: TABLE; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE TABLE channels (
    channelid bigint NOT NULL,
    channel character varying(255) NOT NULL
);


ALTER TABLE channels OWNER TO cussbot;

--
-- Name: channels_channelid_seq; Type: SEQUENCE; Schema: public; Owner: cussbot
--

CREATE SEQUENCE channels_channelid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE channels_channelid_seq OWNER TO cussbot;

--
-- Name: channels_channelid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cussbot
--

ALTER SEQUENCE channels_channelid_seq OWNED BY channels.channelid;


--
-- Name: nicks; Type: TABLE; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE TABLE nicks (
    nickid bigint NOT NULL,
    nick character varying(255) NOT NULL
);


ALTER TABLE nicks OWNER TO cussbot;

--
-- Name: nicks_nick_seq; Type: SEQUENCE; Schema: public; Owner: cussbot
--

CREATE SEQUENCE nicks_nick_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nicks_nick_seq OWNER TO cussbot;

--
-- Name: nicks_nick_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cussbot
--

ALTER SEQUENCE nicks_nick_seq OWNED BY nicks.nick;


--
-- Name: nicks_nickid_seq; Type: SEQUENCE; Schema: public; Owner: cussbot
--

CREATE SEQUENCE nicks_nickid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE nicks_nickid_seq OWNER TO cussbot;

--
-- Name: nicks_nickid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cussbot
--

ALTER SEQUENCE nicks_nickid_seq OWNED BY nicks.nickid;


SET default_with_oids = false;

--
-- Name: uses; Type: TABLE; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE TABLE uses (
    useid bigint NOT NULL,
    nickid bigint NOT NULL,
    channelid bigint NOT NULL,
    wordid bigint NOT NULL,
    date timestamp without time zone DEFAULT now() NOT NULL,
    word text NOT NULL
);


ALTER TABLE uses OWNER TO cussbot;

--
-- Name: uses_id_seq; Type: SEQUENCE; Schema: public; Owner: cussbot
--

CREATE SEQUENCE uses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE uses_id_seq OWNER TO cussbot;

--
-- Name: uses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cussbot
--

ALTER SEQUENCE uses_id_seq OWNED BY uses.useid;


--
-- Name: words; Type: TABLE; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE TABLE words (
    wordid bigint NOT NULL,
    categoryid bigint NOT NULL,
    word character varying(255) NOT NULL
);


ALTER TABLE words OWNER TO cussbot;

--
-- Name: words_id_seq; Type: SEQUENCE; Schema: public; Owner: cussbot
--

CREATE SEQUENCE words_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE words_id_seq OWNER TO cussbot;

--
-- Name: words_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cussbot
--

ALTER SEQUENCE words_id_seq OWNED BY words.wordid;


--
-- Name: categoryid; Type: DEFAULT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY categories ALTER COLUMN categoryid SET DEFAULT nextval('categories_id_seq'::regclass);


--
-- Name: channelid; Type: DEFAULT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY channels ALTER COLUMN channelid SET DEFAULT nextval('channels_channelid_seq'::regclass);


--
-- Name: nickid; Type: DEFAULT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY nicks ALTER COLUMN nickid SET DEFAULT nextval('nicks_nickid_seq'::regclass);


--
-- Name: useid; Type: DEFAULT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY uses ALTER COLUMN useid SET DEFAULT nextval('uses_id_seq'::regclass);


--
-- Name: wordid; Type: DEFAULT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY words ALTER COLUMN wordid SET DEFAULT nextval('words_id_seq'::regclass);


--
-- Name: categories_fullname_key; Type: CONSTRAINT; Schema: public; Owner: cussbot; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_fullname_key UNIQUE (fullname);


--
-- Name: categories_pkey; Type: CONSTRAINT; Schema: public; Owner: cussbot; Tablespace: 
--

ALTER TABLE ONLY categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (categoryid);


--
-- Name: channels_name_key; Type: CONSTRAINT; Schema: public; Owner: cussbot; Tablespace: 
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_name_key UNIQUE (channel);


--
-- Name: channels_pkey; Type: CONSTRAINT; Schema: public; Owner: cussbot; Tablespace: 
--

ALTER TABLE ONLY channels
    ADD CONSTRAINT channels_pkey PRIMARY KEY (channelid);


--
-- Name: nicks_nick_key; Type: CONSTRAINT; Schema: public; Owner: cussbot; Tablespace: 
--

ALTER TABLE ONLY nicks
    ADD CONSTRAINT nicks_nick_key UNIQUE (nick);


--
-- Name: nicks_pkey; Type: CONSTRAINT; Schema: public; Owner: cussbot; Tablespace: 
--

ALTER TABLE ONLY nicks
    ADD CONSTRAINT nicks_pkey PRIMARY KEY (nickid);


--
-- Name: uses_pkey; Type: CONSTRAINT; Schema: public; Owner: cussbot; Tablespace: 
--

ALTER TABLE ONLY uses
    ADD CONSTRAINT uses_pkey PRIMARY KEY (useid);


--
-- Name: words_pkey; Type: CONSTRAINT; Schema: public; Owner: cussbot; Tablespace: 
--

ALTER TABLE ONLY words
    ADD CONSTRAINT words_pkey PRIMARY KEY (wordid);


--
-- Name: categories_shortname_key; Type: INDEX; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE UNIQUE INDEX categories_shortname_key ON categories USING btree (lower((shortname)::text));


--
-- Name: uses_date_key; Type: INDEX; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE INDEX uses_date_key ON uses USING btree (date);


--
-- Name: uses_wordid_key; Type: INDEX; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE INDEX uses_wordid_key ON uses USING btree (wordid);


--
-- Name: words_categoryid_key; Type: INDEX; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE INDEX words_categoryid_key ON words USING btree (categoryid);


--
-- Name: words_word_key; Type: INDEX; Schema: public; Owner: cussbot; Tablespace: 
--

CREATE UNIQUE INDEX words_word_key ON words USING btree (lower((word)::text));


--
-- Name: uses_channelid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY uses
    ADD CONSTRAINT uses_channelid_fkey FOREIGN KEY (channelid) REFERENCES channels(channelid) ON DELETE CASCADE;


--
-- Name: uses_nickid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY uses
    ADD CONSTRAINT uses_nickid_fkey FOREIGN KEY (nickid) REFERENCES nicks(nickid) ON DELETE CASCADE;


--
-- Name: uses_wordid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY uses
    ADD CONSTRAINT uses_wordid_fkey FOREIGN KEY (wordid) REFERENCES words(wordid) ON DELETE CASCADE;


--
-- Name: words_categoryid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: cussbot
--

ALTER TABLE ONLY words
    ADD CONSTRAINT words_categoryid_fkey FOREIGN KEY (categoryid) REFERENCES categories(categoryid) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO cussbot;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
