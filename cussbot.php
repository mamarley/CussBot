#!/usr/bin/php
<?php
/*  cussbot.php - An IRC bot to track how much users cuss.
    Copyright © 2016 Michael Marley <michael@michaelmarley.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require_once("config.php");

if($useSyslog){
	openlog($botNick,LOG_CONS,$logFacility);
}

require_once("CussWordStorage.php");

class Config{
	private $admins;
	private $channels;
	
	public function addAdmin($admin){
		$admin=mb_strtolower($admin);
		if(!array_key_exists($admin,$this->admins)){
			$this->admins[$admin]=array();
			$this->saveConfig();
			cussbotLog(LOG_NOTICE,"\"$admin\" added to the admin list.");
			ircAuto("\"$admin\" is now an admin.");
			return true;
		}else{
			cussbotLog(LOG_NOTICE,"\"$admin\" is already an admin, not adding.");
			ircAuto("\"$admin\" is already an admin.");
			return false;
		}
	}
	
	public function removeAdmin($admin){
		$admin=mb_strtolower($admin);
		if(array_key_exists($admin,$this->admins)){
			if($this->getAdminVariable($admin,"immutable")){
				cussbotLog(LOG_NOTICE,"Attempted to remove immutable admin \"$admin\".");
				ircAuto("\"$admin\" cannot be removed.");
				return false;
			}
			if(count($this->admins)==1){
				cussbotLog(LOG_NOTICE,"Attempted to remove last remaining admin \"$admin\".");
				ircAuto("\"$admin\" is the only remaining admin and cannot be removed.");
				return false;
			}
			unset($this->admins[$admin]);
			$this->saveConfig();
			cussbotLog(LOG_INFO,"\"$admin\" successfully removed from the admin list.");
			ircAuto("\"$admin\" is no longer an admin.");
			return false;
		}else{
			cussbotLog(LOG_NOTICE,"Attempted to remove non-admin \"$admin\" from the admin list.");
			ircAuto("\"$admin\" is not an admin.");
			return false;
		}
	}
	
	public function listAdmins(){
		ircAuto("Admin" . (count($this->admins)==1?" is":"s are") . ": " . implode(" ",array_keys($this->admins)));
	}
	
	public function isAdmin($nick){
		$nick=mb_strtolower($nick);
		if(array_key_exists($nick,$this->admins)){
			cussbotLog(LOG_NOTICE,"\"$nick\" successfully authenticated as admin.");
			return true;
		}else{
			cussbotLog(LOG_WARNING,"Non-admin \"$nick\" attempted to authenticate as admin.");
			ircAuto("$nick is not in the sudoers file.  This incident will be reported.",false);
			$this->messageAdmins("NOTICE",array(),"Non-admin \"$nick\" attempted to authenticate as admin.");
			return false;
		}
	}
	
	public function getAdminVariable($admin,$varName){
		$admin=mb_strtolower($admin);
		if(array_key_exists($admin,$this->admins)){
			return $this->admins[$admin][$varName];
		}else{
			return null;
		}
	}
	
	public function setAdminVariable($admin,$varName,$data,$commit=true){
		$admin=mb_strtolower($admin);
		if(array_key_exists($admin,$this->admins)){
			$this->admins[$admin][$varName]=$data;
			if($commit){
				$this->saveConfig();
			}
			return true;
		}else{
			return false;
		}
	}
	
	public function clearAdminVariable($admin,$varName,$commit=true){
		$admin=mb_strtolower($admin);
		if(array_key_exists($admin,$this->admins)&&array_key_exists($varName,$this->admins[$admin])){
			unset($this->admins[$admin][$varName]);
			if($commit){
				$this->saveConfig();
			}
			return true;
		}else{
			return false;
		}
	}
	
	public function messageAdmins($command,$excludeNicks,$message){
		foreach(array_keys($this->admins) as $admin){
			if(in_array($admin,$excludeNicks)){
				continue;
			}
			ircMessage($command,$admin,$message);
		}
	}
	
	public function addChannel($channel,$nick,$sourceChannel,$method){
		$channel=mb_strtolower($channel);
		$nick=mb_strtolower($nick);
		$sourceChannel=mb_strtolower($sourceChannel);
		if(!mb_substr($channel,0,1)=="#"){
			cussbotLog(LOG_WARNING,"Attempted to add invalid channel name \"$channel\".");
			ircAuto("Invalid channel name \"$channel\".");
			return false;
		}
		if($this->isBlacklistedChannel($channel)){
			cussbotLog(LOG_NOTICE,"Join requested for channel \"$channel\" but this channel is blacklisted.");
			ircAuto("Cannot join channel \"$channel\" because it is blacklisted.");
			return false;
		}else if(!$this->isConfiguredChannel($channel)){
			cussbotLog(LOG_NOTICE,"Requesting join for channel \"$channel\".");
			ircAuto("Joining channel \"$channel\".");
			$this->channels[$channel]=array();
			$this->setChannelVariable($channel,"joined",false,false);
			$this->setChannelVariable($channel,"partRequested",false,false);
			$this->setChannelVariable($channel,"requestor",$nick,false);
			$this->setChannelVariable($channel,"joinReason","admin",false);
			$this->setChannelVariable($channel,"sourceChannel",$sourceChannel,false);
			$this->setChannelVariable($channel,"method",$method,false);
			$this->setChannelVariable($channel,"addedTime",microtime(true),false);
			$this->setChannelVariable($channel,"joinResponseReceived",false,false);
			$this->saveConfig();
			$this->joinChannels($channel);
			return true;
		}else{
			if($this->getChannelVariable($channel,"joined")){
				cussbotLog(LOG_NOTICE,"Join requested for already-joined channel \"$channel\".");
				ircAuto("Already in channel \"$channel\".");
				$returnValue=false;
			}else{
				cussbotLog(LOG_NOTICE,"Join requested for configured but not joined channel \"$channel\", re-sending JOIN.");
				ircAuto("Channel \"$channel\" is configured but not joined; attempting to rejoin.");
				$this->setChannelVariable($channel,"sourceChannel",$sourceChannel,false);
				$this->setChannelVariable($channel,"method",$method,false);
				$this->setChannelVariable($channel,"joinResponseReceived",false,false);
				$this->joinChannels($channel);
				$returnValue=true;
			}
			$this->setChannelVariable($channel,"requestor",$nick,false);
			$this->setChannelVariable($channel,"joinReason","admin",false);
			$this->saveConfig();
			return $returnValue;
		}
	}
	
	public function removeChannel($channel){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)){
			if($this->isBlacklistedChannel($channel)){
				cussbotLog(LOG_NOTICE,"Part requested for blacklisted channel \"$channel\".");
				ircAuto("Not in channel \"$channel\" due to blacklist.");
				return false;
			}
			cussbotLog(LOG_NOTICE,"Requesting part for channel \"$channel\".");
			ircAuto("Parting channel \"$channel\".");
			$this->setChannelVariable($channel,"partRequested",true);
			$this->partChannels($channel);
			if(!$this->getChannelVariable($channel,"joined")){
				unset($this->channels[$channel]);
				$this->saveConfig();
			}
			return true;
		}else{
			cussbotLog(LOG_NOTICE,"Attempted to request part for unconfigured channel \"$channel\".");
			ircAuto("Not in channel \"$channel\".");
			return false;
		}
	}
	
	public function blacklistChannel($channel,$nick){
		$channel=mb_strtolower($channel);
		$nick=mb_strtolower($nick);
		if($this->isConfiguredChannel($channel)){
			if($this->isBlacklistedChannel($channel)){
				cussbotLog(LOG_NOTICE,"Received request to blacklist already-blacklisted channel \"$channel\".");
				ircAuto("Channel \"$channel\" is already blacklisted.");
				return false;
			}
		}else{
			if(!mb_substr($channel,0,1)=="#"){
				cussbotLog(LOG_WARNING,"Attempted to blacklist invalid channel name \"$channel\".");
				ircAuto("Invalid channel name \"$channel\".");
				return false;
			}
			$this->channels[$channel]=array();
			$this->setChannelVariable($channel,"joined",false,false);
		}
		$joined=$this->getChannelVariable($channel,"joined");
		cussbotLog(LOG_NOTICE,"Adding channel \"$channel\" to blacklist.");
		ircAuto("Channel \"$channel\" is now blacklisted.");
		if($joined){
			$this->setChannelVariable($channel,"partRequested",true,false);
		}
		$this->setChannelVariable($channel,"blacklisted",true,false);
		$this->setChannelVariable($channel,"blacklistRequestor",$nick,false);
		$this->setChannelVariable($channel,"blacklistedTime",microtime(true),false);
		$this->setChannelVariable($channel,"joinedBeforeBlacklist",$joined,false);
		$this->saveConfig();
		if($joined){
			$this->partChannels($channel,"Channel has been blacklisted.");
		}
		return true;
	}
	
	public function unblacklistChannel($channel){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)){
			if($this->isBlacklistedChannel($channel)){
				cussbotLog(LOG_NOTICE,"Removing channel \"$channel\" from blacklist.");
				ircAuto("Channel \"$channel\" has been removed from the blacklist.");
				$joinedBeforeBlacklist=$this->getChannelVariable($channel,"joinedBeforeBlacklist");
				$this->clearChannelVariable($channel,"blacklisted",false);
				$this->clearChannelVariable($channel,"blacklistRequestor",false);
				$this->clearChannelVariable($channel,"blacklistedTime",false);
				$this->clearChannelVariable($channel,"joinedBeforeBlacklist",false);
				if($joinedBeforeBlacklist){
					$this->joinChannels($channel);
				}else{
					unset($this->channels[$channel]);
				}
				$this->saveConfig();
				return true;
			}else{
				cussbotLog(LOG_NOTICE,"Received request to remove non-blacklisted channel \"$channel\" from blacklist.");
				ircAuto("Channel \"$channel\" is not blacklisted.");
				return false;
			}
		}else{
			cussbotLog(LOG_NOTICE,"Received request to remove unconfigured channel \"$channel\" from blacklist.");
			ircAuto("Channel \"$channel\" is not configured.");
			return false;
		}
	}
	
	public function joinConfiguredChannels(){
		$channelsToJoin=array();
		foreach(array_keys($this->channels) as $channel){
			if(!$this->getChannelVariable($channel,"joined")&&!$this->isBlacklistedChannel($channel)){
				array_push($channelsToJoin,$channel);
			}
		}
		$this->joinChannels($channelsToJoin);
	}
	
	public function resetChannels(){
		foreach(array_keys($this->channels) as $channel){
			if($this->getChannelVariable($channel,"partRequested")){
				unset($this->channels[$channel]);
				continue;
			}
			$this->setChannelVariable($channel,"joined",false,false);
			$this->setChannelVariable($channel,"partRequested",false,false);
			$this->setChannelVariable($channel,"lastJoinedTime",null,false);
			$this->clearChannelVariable($channel,"joinFailureCode",false);
		}
		$this->saveConfig();
	}
	
	public function joinChannels($channels){
		if(!is_array($channels)){
			$channels=array($channels);
		}
		if(count($channels)>0){
			cussbotLog(LOG_NOTICE,"Sending JOIN command for channel" . (count($channels)==1?"":"s") . ": \"" . implode("\",\"",$channels) . "\".");
			$this->joinPartImplode("JOIN",$channels);
		}
	}
	
	public function partChannels($channels,$message=""){
		if(!is_array($channels)){
			$channels=array($channels);
		}
		if(count($channels)>0){
			cussbotLog(LOG_NOTICE,"Sending PART command for channel" . (count($channels)==1?"":"s") . ": \"" . implode("\",\"",$channels) . "\".");
			$this->joinPartImplode("PART",$channels,$message);
		}
	}
	
	public function pruneInactiveChannels($callback){
		$currentTime=microtime(true);
		$channelsToPart=array();
		foreach(array_keys($this->channels) as $channel){
			$joined=$this->getChannelVariable($channel,"joined");
			$lastJoinedTime=$this->getChannelVariable($channel,"lastJoinedTime");
			$lastActivityTime=$this->getChannelVariable($channel,"lastActivityTime");
			if($joined&&$this->getChannelVariable($channel,"autoPruneExclude")){
				continue;
			}
			if($this->getChannelVariable($channel,"joinReason")=="admin"){
				continue;
			}
			if($this->getChannelVariable($channel,"partRequested")){
				continue;
			}
			if($this->isBlacklistedChannel($channel)){
				continue;
			}
			if($joined&&($lastJoinedTime==null||$lastJoinedTime>=($currentTime-$GLOBALS["autoPruneDelay"]))){
				continue;
			}
			if($lastActivityTime==null||$lastActivityTime<=$currentTime-$GLOBALS["autoPruneActivityPeriod"]){
				array_push($channelsToPart,$channel);
			}
		}
		if(count($channelsToPart)>0){
			if($callback!=null){
				$channelsToPart=$callback($channelsToPart);
			}
			if(count($channelsToPart)>0){
				$this->messageAdmins("NOTICE",array(),"Parting channel" . ((count($channelsToPart)==1)?"":"s") . " " . implode(", ",$channelsToPart) . " due to inactivity.");
				foreach($channelsToPart as $channel){
					$this->setChannelVariable($channel,"partRequested",true,false);
					if(!$this->getChannelVariable($channel,"joined")){
						unset($this->channels[$channel]);
					}
				}
				$this->partChannels($channelsToPart,"Parting channel due to inactivity.  If you believe this is an error, please contact " . $GLOBALS["botOperator"] . ".");
				$this->saveConfig();
			}
		}
	}
	
	public function pruneCreatedChannels(){
		$channelsToPart=array();
		foreach(array_keys($this->channels) as $channel){
			$joined=$this->getChannelVariable($channel,"joined");
			if($joined){
				$names=array_keys($this->getChannelVariable($channel,"names"));
				$joinReason=$this->getChannelVariable($channel,"joinReason");
				if($joinReason=="invite"&&(count($names)==0||(count($names)==1&&$names[0]=="chanserv"))){
					array_push($channelsToPart,$channel);
				}
			}
		}
		if(count($channelsToPart)>0){
			$this->messageAdmins("NOTICE",array(),"Parting channel" . ((count($channelsToPart)==1)?"":"s") . " " . implode(", ",$channelsToPart) . " due to being the only user in them.");
			foreach($channelsToPart as $channel){
				$this->setChannelVariable($channel,"partRequested",true,false);
			}
			$this->partChannels($channelsToPart,"Parting channel due to being the only user in them.  If you believe this is an error, please contact " . $GLOBALS["botOperator"] . ".");
			$this->saveConfig();
		}
	}
	
	private function joinPartImplode($command,$channels,$message=""){
		if($command!="PART"){
			$message="";
		}else{
			$message=" :$message";
		}
		$commandString="$command ";
		$firstChannel=true;
		if(count($channels)>0){
			while(count($channels)>0){
				$nextChannel=array_shift($channels);
				if(strlen($nextChannel)>$GLOBALS["maxCommandLength"]){
					cussbotLog(LOG_ERR,"Channel name \"$nextChannel\" exceeds maxCommandLength!  Cannot join/part.");
					continue;
				}
				if(strlen($commandString)+strlen($nextChannel)+strlen($message)+1<$GLOBALS["maxCommandLength"]){
					if(!$firstChannel){
						$commandString.=",";
					}
					$commandString.="$nextChannel";
					$firstChannel=false;
				}else{
					ircCommand($commandString . $message);
					$commandString="$command $nextChannel";
				}
			}
			ircCommand($commandString . $message);
		}
	}
	
	public function isConfiguredChannel($channel){
		$channel=mb_strtolower($channel);
		return array_key_exists($channel,$this->channels);
	}
	
	public function isBlacklistedChannel($channel){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)){
			if($this->getChannelVariable($channel,"blacklisted")){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	public function onJoin($channel,$nick){
		$channel=mb_strtolower($channel);
		if($nick==$GLOBALS["curBotNick"]){
			cussbotLog(LOG_NOTICE,"Received JOIN command for channel \"$channel\".");
			if($this->isBlacklistedChannel($channel)){
				$redirectedFrom=$this->getChannelVariable($channel,"redirectedFrom");
				if($redirectedFrom==null){
					cussbotLog(LOG_WARNING,"Received unexpected JOIN command for blacklisted channel \"$channel\", parting.");
					$this->messageAdmins("NOTICE",array(),"Received unexpected JOIN command for blacklisted channel \"$channel\", parting.");
				}
				$this->partChannels($channel,"Channel is blacklisted.");
				return;
			}else if(!$this->isConfiguredChannel($channel)){
				$this->channels[$channel]=array();
				$this->setChannelVariable($channel,"requestor",null,false);
				$this->setChannelVariable($channel,"joinReason","unsolicited",false);
				$this->setChannelVariable($channel,"addedTime",microtime(true),false);
			}
			$this->setChannelVariable($channel,"joined",true,false);
			$this->setChannelVariable($channel,"partRequested",false,false);
			$this->setChannelVariable($channel,"lastJoinedTime",microtime(true),false);
			$this->setChannelVariable($channel,"joinResponseReceived",true,false);
			$this->clearChannelVariable($channel,"joinFailureCode",false);
			$this->clearChannelVariable($channel,"names",false);
			$this->saveConfig();
		}else{
			$nick=mb_strtolower($nick);
			$currentNamesList=$this->getChannelVariable($channel,"names");
			$currentNamesList[$nick]=null;
			$this->setChannelVariable($channel,"names",$currentNamesList,false);
		}
	}
	
	public function onQuit($nick){
		if($nick==$GLOBALS["curBotNick"]){
			cussbotLog(LOG_WARNING,"Received unexpected QUIT command for own nick, this should never happen.");
		}else{
			$nick=mb_strtolower($nick);
			foreach(array_keys($this->channels) as $channel){
				if(array_key_exists("names",$this->channels[$channel])&&array_key_exists($nick,$this->channels[$channel]["names"])){
					unset($this->channels[$channel]["names"][$nick]);
				}
			}
		}
	}
	
	public function onPart($channel,$nick){
		$channel=mb_strtolower($channel);
		if($nick==$GLOBALS["curBotNick"]){
			if($this->isConfiguredChannel($channel)){
				if($this->isBlacklistedChannel($channel)){
					$this->clearChannelVariable($channel,"names",false);
					cussbotLog(LOG_NOTICE,"Received PART command for blacklisted channel \"$channel\", keeping in configuration.");
				}else if($this->getChannelVariable($channel,"partRequested")){
					cussbotLog(LOG_NOTICE,"Received PART command for channel \"$channel\", removing from configuration.");
					unset($this->channels[$channel]);
					$this->saveConfig();
				}else{
					cussbotLog(LOG_WARNING,"Received unexpected PART command for channel \"$channel\", requesting re-join.");
					$this->setChannelVariable($channel,"joined",false);
					$this->joinChannels($channel);
				}
			}else{
				cussbotLog(LOG_NOTICE,"Received PART command for unconfigured channel \"$channel\", ignoring.");
			}
		}else{
			$nick=mb_strtolower($nick);
			$currentNamesList=$this->getChannelVariable($channel,"names");
			unset($currentNamesList[$nick]);
			$this->setChannelVariable($channel,"names",$currentNamesList,false);
		}
	}
	
	public function onNotInChannel($channel){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)){
			if($this->isBlacklistedChannel($channel)){
				cussbotLog(LOG_NOTICE,"Received Not In Channel response when PARTing blacklisted channel \"$channel\", keeping in configuration.");
			}else if($this->getChannelVariable($channel,"joined")){
				cussbotLog(LOG_ERR,"Received Not In Channel response for currently-JOINed channel \"$channel\", this likely indicates a bug somewhere.");
			}else{
				cussbotLog(LOG_NOTICE,"Received Not In Channel response for channel \"$channel\", removing from configuration.");
				unset($this->channels[$channel]);
				$this->saveConfig();
			}
		}else{
			cussbotLog(LOG_NOTICE,"Received Not In Channel response for unconfigured channel \"$channel\", ignoring.");
		}
	}
	
	public function onKick($channel,$nick){
		$channel=mb_strtolower($channel);
		if($nick==$GLOBALS["curBotNick"]){
			if($this->isConfiguredChannel($channel)){
				cussbotLog(LOG_NOTICE,"Received KICK command for channel \"$channel\", requesting re-join.");
				$this->setChannelVariable($channel,"joined",false);
				$this->clearChannelVariable($channel,"names",false);
				$this->joinChannels($channel);
			}else{
				cussbotLog(LOG_NOTICE,"Received KICK command for unconfigured channel \"$channel\", ignoring.");
			}
		}else{
			$nick=mb_strtolower($nick);
			$currentNamesList=$this->getChannelVariable($channel,"names");
			unset($currentNamesList[$nick]);
			$this->setChannelVariable($channel,"names",$currentNamesList,false);
		}
	}
	
	public function onInvite($channel,$nick){
		$channel=mb_strtolower($channel);
		$nick=mb_strtolower($nick);
		if($this->isBlacklistedChannel($channel)){
			cussbotLog(LOG_NOTICE,"Received INVITE command for blacklisted channel \"$channel\", ignoring.");
			ircNotice("Channel \"$channel\" is blacklisted.",$nick);
			$this->messageAdmins("NOTICE",array($nick),"User \"$nick\" sent INVITE for blacklisted channel \"$channel\".");
			return false;
		}else if($this->isConfiguredChannel($channel)){
			if($this->getChannelVariable($channel,"joined")){
				cussbotLog(LOG_NOTICE,"INVITE received for already-joined channel \"$channel\".");
				ircAuto("Already in channel \"$channel\".");
				$returnValue=false;
			}else{
				cussbotLog(LOG_NOTICE,"INVITE received for configured but not joined channel \"$channel\", re-sending JOIN.");
				ircAuto("Channel \"$channel\" is configured but not joined; attempting to rejoin.");
				$this->setChannelVariable($channel,"sourceChannel",null,false);
				$this->setChannelVariable($channel,"method",$method,false);
				$this->setChannelVariable($channel,"joinResponseReceived",false,false);
				$this->joinChannels($channel);
				$returnValue=true;
			}
			$this->setChannelVariable($channel,"requestor",$nick,false);
			$this->saveConfig();
			return $returnValue;
		}else{
			cussbotLog(LOG_NOTICE,"Received INVITE command for channel \"$channel\" from nick \"$nick\", requesting join.");
			$this->channels[$channel]=array();
			$this->setChannelVariable($channel,"joined",false,false);
			$this->setChannelVariable($channel,"partRequested",false,false);
			$this->setChannelVariable($channel,"requestor",$nick,false);
			$this->setChannelVariable($channel,"joinReason","invite",false);
			$this->setChannelVariable($channel,"sourceChannel",null,false);
			$this->setChannelVariable($channel,"method","NOTICE",false);
			$this->setChannelVariable($channel,"addedTime",microtime(true),false);
			$this->setChannelVariable($channel,"joinResponseReceived",false,false);
			$this->saveConfig();
			$this->joinChannels($channel);
			return true;
		}
	}
	
	public function onJoinFailure($channel,$errorCode,$errorDescription){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)){
			$joinReason=$this->getChannelVariable($channel,"joinReason");
			$requestor=$this->getChannelVariable($channel,"requestor");
			$target=$this->getChannelVariable($channel,"sourceChannel");
			$command=$this->getChannelVariable($channel,"method");
			if($target==$channel||!$this->getChannelVariable($target,"joined")){
				$target=$requestor;
			}
			if($errorCode==000){
				$action="will not retry";
				$delete=false;
			}else if($errorCode==471||$errorCode==474||$errorCode==480){
				$action="will retry later";
				$delete=false;
			}else{
				$action="removing channel from configuration";
				$delete=true;
			}
			cussbotLog(LOG_WARNING,"Error $errorCode ($errorDescription) joining channel \"$channel\", $action.");
			if($this->getChannelVariable($channel,"joinFailureCode")!==$errorCode){
				$save=true;
				if($target!=null&&!$this->getChannelVariable($channel,"joinResponseReceived")){
					$this->setChannelVariable($channel,"joinResponseReceived",true,false);
					ircMessage($command!=null?$command:"PRIVMSG",$target,($requestor!=null?"$requestor: ":"") . "Error $errorCode ($errorDescription) joining channel \"$channel\", $action.");
				}
				$this->messageAdmins("NOTICE",array($requestor),"Error $errorCode ($errorDescription) joining channel \"$channel\" (joined using \"$joinReason\" by \"$requestor\"), $action.");
			}else{
				$save=false;
			}
			if($delete){
				unset($this->channels[$channel]);
				$this->saveConfig();
			}else{
				$this->setChannelVariable($channel,"joinFailureCode",$errorCode,$save);
			}
		}
	}
	
	public function onJoinRedirect($from,$to,$errorCode,$errorDescription){
		$from=mb_strtolower($from);
		$to=mb_strtolower($to);
		$returnValue=true;
		if($this->isConfiguredChannel($from)){
			if($this->isBlacklistedChannel($to)){
				$nick=$this->getChannelVariable($from,"requestor");
				$this->onJoinFailure($from,000,"Channel \"$from\" redirected to blacklisted channel \"$to\"");
				$this->blacklistChannel($from,"auto");
				$returnValue=false;
			}else{
				cussbotLog(LOG_NOTICE,"Channel \"$from\" redirected to \"$to\" ($errorCode: $errorDescription), renaming channel in configuration.");
				$this->setChannelVariable($from,"redirectedFrom",$from,false);
				$this->channels[$to]=$this->channels[$from];
				unset($this->channels[$from]);
			}
			$this->saveConfig();
		}
		return $returnValue;
	}
	
	public function onNamesList($channel,$namesList){
		$channel=mb_strtolower($channel);
		foreach($namesList as $key=>$value){
			$namesList[$key]=mb_strtolower($value);
		}
		$currentNamesList=$this->getChannelVariable($channel,"names");
		if($currentNamesList==null){
			$currentNamesList=$namesList;
		}else{
			$currentNamesList=array_merge($currentNamesList,$namesList);
		}
		$this->setChannelVariable($channel,"names",$currentNamesList,false);
	}
	
	public function onEndNamesList($channel){
		$channel=mb_strtolower($channel);
	}
	
	public function onNick($oldNick,$newNick){
		$oldNick=mb_strtolower($oldNick);
		$newNick=mb_strtolower($newNick);
		foreach(array_keys($this->channels) as $channel){
			if(array_key_exists("names",$this->channels[$channel])&&array_key_exists($oldNick,$this->channels[$channel]["names"])){
				$this->channels[$channel]["names"][$newNick]=$this->channels[$channel]["names"][$oldNick];
				unset($this->channels[$channel]["names"][$oldNick]);
			}
		}
	}
	
	public function onMessage($command,$sender,$target,$message){
		if($this->isConfiguredChannel($target)&&$this->getChannelVariable($target,"joined")&&$sender!=null&&$sender!=""&&$sender!=$GLOBALS["curBotNick"]){
			$this->setChannelVariable($target,"lastActivityTime",microtime(true),false);
		}
	}
	
	public function listChannels(){
		$channelArray=array();
		foreach(array_keys($this->channels) as $channel){
			if(!$this->isBlacklistedChannel($channel)){
				if($this->getChannelVariable($channel,"joined")==true){
					array_push($channelArray,"\x0309". $channel . "\x03");
				}else{
					array_push($channelArray,"\x0304". $channel . "\x03");
				}
			}
		}
		ircAuto("Currently configured channel" . (count($channelArray)==1?"":"s") . ": " . implode(" ",$channelArray));
	}
	
	public function listChannelBlacklist(){
		$channelArray=array();
		foreach(array_keys($this->channels) as $channel){
			if($this->isBlacklistedChannel($channel)){
				array_push($channelArray,"\x0304". $channel . "\x03");
			}
		}
		ircAuto("Currently blacklisted channel" . (count($channelArray)==1?"":"s") . ": " . implode(" ",$channelArray));
	}
	
	public function channelInfo($channel){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)){
			$replyArray=array();
			foreach($this->channels[$channel] as $key=>$value){
				if(!str_endswith(mb_strtolower($key),"time")){
					array_push($replyArray,"\"$key\":\"$value\"");
				}else{
					array_push($replyArray," \"$key\":\"" . date("Ymd H:i:s T",$value) . "\"");
				}
			}
			ircAuto("Configuration for channel \"$channel\" :" . implode(", ",$replyArray));
		}else{
			ircAuto("Channel \"$channel\" is not configured.");
		}
	}
	
	public function getChannelVariable($channel,$varName){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)&&array_key_exists($varName,$this->channels[$channel])){
			return $this->channels[$channel][$varName];
		}else{
			return null;
		}
	}
	
	public function setChannelVariable($channel,$varName,$data,$commit=true){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)){
			$this->channels[$channel][$varName]=$data;
			if($commit){
				$this->saveConfig();
			}
			return true;
		}else{
			return false;
		}
	}
	
	public function clearChannelVariable($channel,$varName,$commit=true){
		$channel=mb_strtolower($channel);
		if($this->isConfiguredChannel($channel)&&array_key_exists($varName,$this->channels[$channel])){
			unset($this->channels[$channel][$varName]);
			if($commit){
				$this->saveConfig();
			}
			return true;
		}else{
			return false;
		}
	}
	
	private function createNewConfig(){
		cussbotLog(LOG_NOTICE,"Creating new configuration file, the bot will join channel \"#botters-test\" automatically.");
		$this->admins=array();
		$this->admins[$GLOBALS["botOperator"]]=array("immutable"=>true);
		$this->channels=array();
		$this->channels["#botters-test"]=array();
		$this->saveConfig();
	}
	
	public function loadConfig(){
		if(file_exists($GLOBALS["configFile"])){
			cussbotLog(LOG_NOTICE,"Loading existing configuration file.");
			$loadedConfig=json_decode(file_get_contents($GLOBALS["configFile"]),true);
			if($loadedConfig!==null){
				$this->admins=$loadedConfig["admins"];
				$this->channels=$loadedConfig["channels"];
			}else{
				cussbotLog(LOG_CRIT,"Configuration file was malformed and could not be loaded!");
				exit(1);
			}
		}else{
			$this->createNewConfig();
		}
	}
	
	public function saveConfig(){
		$objectVars=get_object_vars($this);
		foreach(array_keys($objectVars["channels"]) as $channel){
			unset($objectVars["channels"][$channel]["names"]);
		}
		$fileSaveResult=file_put_contents($GLOBALS["configFile"],json_encode($objectVars,JSON_PRETTY_PRINT));
		if($fileSaveResult===false){
			cussbotLog(LOG_ERR,"Failed to save configuration file! Check permissions and disk free space.");
		}else{
			cussbotLog(LOG_INFO,"Configuration file saved successfully.");
		}
	}
}

$syslogLevels=array("EMERGENCY","ALERT","CRITICAL","ERROR","WARNING","NOTICE","INFO","DEBUG");

mb_internal_encoding($encoding);

$config=new Config();
$config->loadConfig();

$cussWordStorage=new CussWordStorage($sqlHostname,$sqlPort,$sqlUsername,$sqlPassword,$sqlDatabaseName);

$ircServerIndex=0;
$nullArray=null;
$maxCommandLength-=2;

resetState();

ircConnect();

while(true){
	$connectionStreams=array($ircConnection);
	$streamSelectResult=stream_select($connectionStreams,$nullArray,$nullArray,0,max(($nextTimeoutTime-microtime(true))*1000000,0));
	if($streamSelectResult===false){
		cussbotLog(LOG_WARNING,"stream_select() returned false, that is odd.");
		continue;
	}else if($streamSelectResult>0&&count($connectionStreams)===0){
		cussbotLog(LOG_WARNING,"stream_select() returned indicated data available when none was, reconnecting!");
		ircReconnect();
		continue;
	}
	$currentTime=microtime(true);
	if($lastPingTime<=($currentTime-$pingInterval)){
		cussbotLog(LOG_INFO,"Timer expired, executing timed tasks…");
		$lastPingTime=$currentTime;
		$cussWordStorage->ping();
		if($authenticated&&$currentTime>$lastConnectionTime+$maxPongsMissed*$pingInterval){
			$config->pruneInactiveChannels(function($channelsToPart) use (&$config,&$cussWordStorage){
				$statsResults=$cussWordStorage->getStats("channelstats",array(),array(),$channelsToPart,array(),null);
				$statsArray=array();
				foreach($statsResults as $statsResult){
					$statsArray[$statsResult["channel"]]=$statsResult["count"];
				}
				foreach($channelsToPart as $key=>$channel){
					if($config->getChannelVariable($channel,"joined")&&array_key_exists($channel,$statsArray)&&$statsArray[$channel]>0){
						$config->setChannelVariable($channel,"autoPruneExclude",true,false);
						unset($channelsToPart[$key]);
					}
				}
				return $channelsToPart;
			});
			$config->pruneCreatedChannels();
		}
		if($pongsMissed>$maxPongsMissed){
			cussbotLog(LOG_WARNING,"Too many pongs missed, reconnecting!");
			ircReconnect();
			continue;
		}else{
			if($authenticated){
				ircCommandJumpQueue("PING " . round(microtime(true)*1000));
				if($curBotNick!=$botNick){
					ircCommand("NICK " . $botNick);
				}
				$config->joinConfiguredChannels();
			}
			$pongsMissed++;
		}
		if(count($retransmitQueue)>0&&count($transmitQueue)==0){
			ircCommand(array_shift($retransmitQueue));
		}
	}
	if(count($transmitQueue)>0){
		ircCommandNoRateLimit(array_shift($transmitQueue));
		$nextTimeoutTime=$currentTime+$rateLimitInterval;
	}else{
		$nextTimeoutTime=$currentTime+$pingInterval;
	}
	if(in_array($ircConnection,$connectionStreams)){
		if(feof($ircConnection)){
			cussbotLog(LOG_WARNING,"feof() returned true, reconnecting!");
			ircReconnect();
			continue;
		}
		$line=fgets($ircConnection);
		if($line===false){
			cussbotLog(LOG_WARNING,"fgets() returned false, reconnecting!");
			ircReconnect();
			continue;
		}
		if(!str_endswith($line,"\n")){
			$incompleteLinesReceived++;
			cussbotLog(LOG_INFO,"Incomplete line detected, $incompleteLinesReceived in a row.");
			if($incompleteLine===null){
				$incompleteLine=trim($line);
			}else{
				$incompleteLine.=trim($line);
			}
			if($incompleteLinesReceived>$maxIncompleteLines){
				cussbotLog(LOG_WARNING,"Too many incomplete lines received in row, reconnecting!");
				ircReconnect();
			}
			continue;
		}else if($incompleteLine!==null){
			$incompleteLinesReceived=0;
			$line=$incompleteLine . trim($line);
			$incompleteLine=null;
		}
		$line=trim($line);
		if($line===""){
			cussbotLog(LOG_DEBUG,"Empty line received, this is a bit strange.");
			continue;
		}
		cussbotLog(LOG_DEBUG,"IRC RECV: \"$line\"");
		$pongsMissed=0;
		$lineArray=explode(" :",trim($line),2);
		$prefixArray=explode(" ",$lineArray[0]);
		if(count($prefixArray)>=2){
			$command=$prefixArray[1];
		}else{
			$command=$prefixArray[0];
		}
		$senderArray=explode("!",$prefixArray[0]);
		if(count($senderArray)==2){
			$sender=trim(mb_substr($senderArray[0],1));
		}else{
			$sender="";
		}
		if(count($prefixArray)>=3){
			$target=$prefixArray[2];
			if($target==$curBotNick){
				$target=$sender;
			}else if(mb_substr($target,0,1)=="+"||mb_substr($target,0,1)=="@"){
				$target=mb_substr($target,1);
			}
		}else{
			$target="";
		}
		$config->onMessage($command,$sender,$target,isset($lineArray[1])?$lineArray[1]:null);
		if($command=="PRIVMSG"||$command=="NOTICE"){
			if($sender==$curBotNick){
				cussbotLog(LOG_WARNING,"Received unexpected self-sent message, ignoring.");
				continue;
			}
			if(count($lineArray)==2){
				$message=stripIrcFormatting($lineArray[1]);
				if(str_startswith($message,"\001")&&str_endswith($message,"\001")){
					$ctcp=true;
					$message=str_replace("\001","",$message);
				}else{
					$ctcp=false;
				}
				if($sender=="NickServ"||$sender=="ChanServ"){
					continue;
				}else if($sender=="MemoServ"){
					if((str_startswith($message,"You have ")&&str_endswith($message," new memo."))||str_startswith($message,"You have a new memo from")){
						ircMessage("PRIVMSG",$sender,"READ NEW");
						ircMessage("PRIVMSG",$sender,"DELETE ALL");
						$memoServData=null;
					}else if($memoServData!=null){
						if($message!="------------------------------------------"){
							$config->messageAdmins("NOTICE",array(),"MemoServ message sent at $memoServData[1] by $memoServData[0]: $message");
							$memoServData=null;
						}
					}else if(strpos($message," - Sent by ")){
						$memoServData=explode(", ",explode(" - Sent by ",$message,2)[1],2);
					}
					continue;
				}
				if(str_startswith(mb_strtolower($message),mb_strtolower($curBotNick) . ":")){
					$botCommandString=str_ireplace("$curBotNick:","",$message);
				}else if(str_startswith(mb_strtolower($message),mb_strtolower($curBotNick) . ",")){
					$botCommandString=str_ireplace("$curBotNick,","",$message);
				}else if($target==$sender){
					$botCommandString=$message;
				}else{
					$botCommandString=null;
				}
				if(isset($botCommandString)){
					$botCommandString=removeDupeSpaces($botCommandString);
					$botCommandArguments=explode(" ",trim($botCommandString));
					$botCommand=mb_strtolower(array_shift($botCommandArguments));
					$botCommandArgumentsLength=count($botCommandArguments);
					if($ctcp&&$botCommand=="ping"){
						if($botCommandArgumentsLength==1){
							ircNotice(wrapCtcp("PING " . $botCommandArguments[0]));
						}else{
							ircNotice(wrapCtcp("ERRMSG Invalid CTCP command!  Syntax is \"PING <timestamp>\"."));
						}
					}else if($ctcp&&$botCommand=="version"){
						if($botCommandArgumentsLength==0){
							ircNotice(wrapCtcp("VERSION worthlessbot PHP IRC bot:git:PHP " . phpversion() . " " . php_uname("srm") . " " . php_uname("r") . " " . php_uname("m")));
						}else{
							ircNotice(wrapCtcp("ERRMSG Invalid CTCP command!  Syntax is \"VERSION\"."));
						}
					}else if($ctcp&&$botCommand=="time"){
						if($botCommandArgumentsLength==0){
							ircNotice(wrapCtcp("TIME " . date("D M d H:i:s Y T")));
						}else{
							ircNotice(wrapCtcp("ERRMSG Invalid CTCP command!  Syntax is \"TIME\"."));
						}
					}else if($ctcp&&$botCommand=="clientinfo"){
						ircNotice(wrapCtcp("CLIENTINFO CLIENTINFO PING SOURCE TIME USERINFO VERSION"));
					}else if($ctcp&&$botCommand=="userinfo"){
						if($botCommandArgumentsLength==0){
							ircNotice(wrapCtcp("USERINFO $botName"));
						}else{
							ircNotice(wrapCtcp("ERRMSG Invalid CTCP command!  Syntax is \"USERINFO\"."));
						}
					}else if($ctcp&&$botCommand=="source"){
						if($botCommandArgumentsLength==0){
							ircNotice(wrapCtcp("ERRMSG Source not available over FTP, use \"$botName: source\" for a URL."));
						}else{
							ircNotice(wrapCtcp("ERRMSG Invalid CTCP command!  Syntax is \"SOURCE\"."));
						}
					}else if($ctcp){
						ircNotice(wrapCtcp("ERRMSG Invalid or unsupported CTCP command!"));
					}else if($botCommand=="help"){
						if($botCommandArgumentsLength==0){
							ircAuto("Valid commands are \"help\", \"categorystats [<#channel>]… [<nick>]…\", \"channelstats [<#channel>]… [<nick>]…\", \"nickstats [<#channel>]… [<nick>]…\", \"wordstats [<#channel>]… [<nick>]…\", \"webstats\", \"listadmins\", \"addmin <nick>\", \"removemin <nick>\", \"listchannels\", \"channelinfo <#channel>\", \"join <#channel>\", \"part <#channel>\", \"listchannelblacklist\", \"blacklistchannel <#channel>\", \"unblacklistchannel <#channel>\", \"addword <category> <word>\", \"removeword <word>\", \"listcategories\", \"addcategory <shortname> <fullname>\", \"removecategory <name>\", \"listwordwhitelist\", \"whitelistword <word>\", \"unwhitelistword <word>\", \"refreshwords\", \"source\", \"logVerbose <true|false>\", \"raw <IRC command>\".");
						}else{
							ircAuto("Invalid command!  Syntax is \"help\".");
						}
					}else if($botCommand=="source"){
						if($botCommandArgumentsLength==0){
							ircAuto("https://github.com/mamarley/CussBot");
						}else{
							ircAuto("Invalid command!  Syntax is \"source\".");
						}
					}else if($botCommand=="categorystats"||$botCommand=="stats"||$botCommand=="channelstats"||$botCommand=="nickstats"||$botCommand=="wordstats"||$botCommand=="mostcommon"){
						$channelNotInArgs=array();
						$nickNotInArgs=array();
						$channelInArgs=array();
						$nickInArgs=array();
						
						if($botCommand=="stats"){
							$botCommand="categorystats";
						}else if($botCommand=="mostcommon"){
							$botCommand="wordstats";
						}
						
						foreach($botCommandArguments as $argument){
							if(mb_substr($argument,0,1)=="-"){
								$argument=mb_substr($argument,1);
								if(mb_substr($argument,0,1)=="#"){
									array_push($channelNotInArgs,$argument);
								}else{
									array_push($nickNotInArgs,$argument);
								}
							}else{
								if(mb_substr($argument,0,1)=="+"){
									$argument=mb_substr($argument,1);
								}
								if(mb_substr($argument,0,1)=="#"){
									array_push($channelInArgs,$argument);
								}else{
									array_push($nickInArgs,$argument);
								}
							}
						}
						
						try{
							$statsResults=$cussWordStorage->getStats($botCommand,$channelNotInArgs,$nickNotInArgs,$channelInArgs,$nickInArgs,($botCommand!="stats")?$statsCountLimit:null);
							
							if(count($statsResults)>0){
								$response=array();
								if($botCommand=="categorystats"){
									foreach($statsResults as $statsResult){
										array_push($response,$statsResult["count"] . " " . $statsResult["fullname"] . ((int)$statsResult["count"]!=1 ? "s":""));
									}
								}else if($botCommand=="channelstats"){
									foreach($statsResults as $statsResult){
										array_push($response,$statsResult["channel"] . " with " . $statsResult["count"] . " cuss" . ((int)$statsResult["count"]!=1 ? "es":""));
									}
								}else if($botCommand=="nickstats"){
									foreach($statsResults as $statsResult){
										array_push($response,substr_replace($statsResult["nick"],"\u{FEFF}",1,0) . " with " . $statsResult["count"] . " cuss" . ((int)$statsResult["count"]!=1 ? "es":""));
									}
								}else if($botCommand=="wordstats"){
									foreach($statsResults as $statsResult){
										array_push($response,substr_replace($statsResult["word"],"\u{FEFF}",1,0) . " with " . $statsResult["count"] . " use" . ((int)$statsResult["count"]!=1 ? "s":""));
									}
								}
								ircAuto(implode(", ",$response));
							}else{
								ircAuto("No results.");
							}
						}catch(Exception $e){
							cussbotLog(LOG_ERR,"Failed to retrieve statistics with message \"" . $e->getMessage() . "\"!");
							ircAuto("Error while retrieving statistics!");
						}
					}else if($botCommand=="webstats"){
						if($botCommandArgumentsLength==0){
							ircAuto("https://michaelmarley.com/cussbotstatistics");
						}else{
							ircAuto("Invalid command!  Syntax is \"webstats\".");
						}
					}else if($botCommand=="listadmins"){
						if($botCommandArgumentsLength==0){
							$config->listAdmins();
						}else{
							ircAuto("Invalid command!  Syntax is \"listadmins\".");
						}
					}else if($botCommand=="addmin"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								$config->addAdmin($botCommandArguments[0]);
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"addmin <nick>\".");
						}
					}else if($botCommand=="removemin"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								$config->removeAdmin($botCommandArguments[0]);
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"removemin <nick>\".");
						}
					}else if($botCommand=="listchannels"){
						if($botCommandArgumentsLength==0){
							$config->listChannels();
						}else{
							ircAuto("Invalid command!  Syntax is \"listchannels\".");
						}
					}else if($botCommand=="channelinfo"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								$config->channelInfo($botCommandArguments[0]);
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"part <#channel>\".");
						}
					}else if($botCommand=="join"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								$config->addChannel($botCommandArguments[0],$sender,$target,$command);
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"join <#channel>\".");
						}
					}else if($botCommand=="part"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								$config->removeChannel($botCommandArguments[0]);
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"part <#channel>\".");
						}
					}else if($botCommand=="listchannelblacklist"){
						if($botCommandArgumentsLength==0){
							$config->listChannelBlacklist();
						}else{
							ircAuto("Invalid command!  Syntax is \"listchannels\".");
						}
					}else if($botCommand=="blacklistchannel"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								$config->blacklistChannel($botCommandArguments[0],$sender);
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"join <#channel>\".");
						}
					}else if($botCommand=="unblacklistchannel"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								$config->unblacklistChannel($botCommandArguments[0]);
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"part <#channel>\".");
						}
					}else if($botCommand=="raw"){
						if($config->isAdmin($sender)){
							ircCommand(implode(" ",$botCommandArguments));
						}
					}else if($botCommand=="logverbose"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								switch($botCommandArguments[0]){
									case "true":
										$logVerbose=true;
										ircAuto("Verbose logging enabled.");
										break;
									case "false":
										$logVerbose=false;
										ircAuto("Verbose logging disabled.");
										break;
									default:
										ircAuto("Invalid command!  Syntax is \"logVerbose <true|false>\".");
								}
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"logVerbose <true|false>\".");
						}
					}else if($botCommand=="addword"){
						if($botCommandArgumentsLength==2){
							if($config->isAdmin($sender)){
								try{
									if($cussWordStorage->addWord($botCommandArguments[0],$botCommandArguments[1])){
										ircAuto("\"" . $botCommandArguments[1] . "\" is now recognized as a cuss word.");
									}else{
										ircAuto("\"" . $botCommandArguments[1] . "\" could not be added.  Perhaps it was already added or the category does not exist.");
									}
								}catch(Exception $e){
									cussbotLog(LOG_ERR,"Failed to insert word with message \"" . $e->getMessage() . "\".");
									ircAuto("Error while inserting word!");
								}
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"addword <category> <word>\".");
						}
					}else if($botCommand=="removeword"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								try{
									if($cussWordStorage->removeWord($botCommandArguments[0])){
										ircAuto("\"" . $botCommandArguments[0] . "\" is no longer recognized as a cuss word.");
									}else{
										ircAuto("\"" . $botCommandArguments[0] . "\" is not recognized as a cuss word.");
									}
								}catch(Exception $e){
									cussbotLog(LOG_ERR,"Failed to delete word with message \"" . $e->getMessage() . "\".");
									ircAuto("Error while deleting word!");
								}
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"removeword <word>\".");
						}
					}else if($botCommand=="refreshwords"){
						if($botCommandArgumentsLength==0){
							try{
								$cussWordStorage->loadCussWords();
								$cussWordStorage->loadWhitelistWords();
								ircAuto("Word list refreshed");
							}catch(Exception $e){
								cussbotLog(LOG_ERR,"Failed to reload word list.");
								ircAuto("Error while reloading word list!");
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"refreshwords\".");
						}
					}else if($botCommand=="listcategories"){
						if($botCommandArgumentsLength==0){
							try{
								$categories=$cussWordStorage->getCategories();
								$response="Known cuss categories are: ";
								foreach($categories as $category){
									$response.="\"" . $category["shortname"] . "\" ";
								}
								ircAuto($response);
							}catch(Exception $e){
								cussbotLog(LOG_ERR,"Failed to get category list with message \"" . $e->getMessage() . "\".");
								ircAuto("Error while getting category list!");
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"listcategories\".");
						}
					}else if($botCommand=="addcategory"){
						if($botCommandArgumentsLength>=2){
							if($config->isAdmin($sender)){
								try{
									if($cussWordStorage->addCategory($botCommandArguments[0],implode(" ",array_slice($botCommandArguments,1)))){
										ircAuto("\"" . $botCommandArguments[0] . "\" is now recognized as a cuss category.");
									}else{
										ircAuto("\"" . $botCommandArguments[0] . "\" is already recognized as a cuss category.");
									}
								}catch(Exception $e){
									cussbotLog(LOG_ERR,"Failed to insert category with message \"" . $e->getMessage() . "\".");
									ircAuto("Error while inserting category!");
								}
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"addcategory <shortname> <fullname>\".");
						}
					}else if($botCommand=="removecategory"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								try{
									if($cussWordStorage->removeCategory($botCommandArguments[0])){
										ircAuto("\"" . $botCommandArguments[0] . "\" is no longer recognized as a cuss category.");
									}else{
										ircAuto("\"" . $botCommandArguments[0] . "\" is not recognized as a cuss category.");
									}
								}catch(Exception $e){
									cussbotLog(LOG_ERR,"Failed to delete category with message \"" . $e->getMessage() . "\".");
									ircAuto("Error while deleting category!");
								}
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"removecategory <shortname>\".");
						}
					}else if($botCommand=="listwordwhitelist"){
						if($botCommandArgumentsLength==0){
							try{
								$whitelistWords=$cussWordStorage->getWhitelistWords();
								$response="Whitelisted words are: ";
								foreach($whitelistWords as $whitelistWord){
									$response.="\"" . $whitelistWord["whitelistword"] . "\" ";
								}
								ircAuto($response);
							}catch(Exception $e){
								cussbotLog(LOG_ERR,"Failed to get whitelist words with message \"" . $e->getMessage() . "\".");
								ircAuto("Error while getting whitelist words!");
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"listwordwhitelist\".");
						}
					}else if($botCommand=="whitelistword"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								try{
									if($cussWordStorage->addWhitelistWord($botCommandArguments[0])){
										ircAuto("\"" . $botCommandArguments[0] . "\" is now whitelisted.");
									}else{
										ircAuto("\"" . $botCommandArguments[0] . "\" is already whitelisted.");
									}
								}catch(Exception $e){
									cussbotLog(LOG_ERR,"Failed to insert whitelist word with message \"" . $e->getMessage() . "\".");
									ircAuto("Error while inserting whitelist word!");
								}
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"whitelistword <word>\".");
						}
					}else if($botCommand=="unwhitelistword"){
						if($botCommandArgumentsLength==1){
							if($config->isAdmin($sender)){
								try{
									if($cussWordStorage->removeWhitelistWord($botCommandArguments[0])){
										ircAuto("\"" . $botCommandArguments[0] . "\" is no longer whitelisted.");
									}else{
										ircAuto("\"" . $botCommandArguments[0] . "\" is not whitelisted.");
									}
								}catch(Exception $e){
									cussbotLog(LOG_ERR,"Failed to delete whitelist word with message \"" . $e->getMessage() . "\".");
									ircAuto("Error while deleting whitelist word!");
								}
							}
						}else{
							ircAuto("Invalid command!  Syntax is \"unwhitelistword <word>\".");
						}
					}else{
						ircAuto("Invalid command!  Try \"help\".");
					}
				}else{
					try{
						$cussWordStorage->onMessage($command,$sender,$target,$message);
					}catch(Exception $e){
						cussbotLog(LOG_ERR,"Failed to analyze message with message \"" . $e->getMessage() . "\".");
					}
				}
			}
		}else if($command=="KICK"){
			$config->onKick($prefixArray[2],$prefixArray[3]);
		}else if($command=="PART"){
			$config->onPart($prefixArray[2],$sender);
		}else if($command=="JOIN"){
			$config->onJoin($prefixArray[2],$sender);
		}else if($command=="QUIT"){
			$config->onQuit($sender);
		}else if($command=="INVITE"){
			if($prefixArray[2]==$curBotNick){
				$config->onInvite($lineArray[1],$sender);
			}
		}else if($command=="NICK"){
			if($sender==$curBotNick){
				cussbotLog(LOG_NOTICE,"Received NICK command, changing nick to \"$lineArray[1]\".");
				$curBotNick=$lineArray[1];
			}else{
				$config->onNick($sender,$lineArray[1]);
			}
		}else if($command=="ERROR"){
			cussbotLog(LOG_WARNING,"Received ERROR command, attempting to reconnect…");
			ircReconnect();
		}else if($command=="PING"){
			ircCommandJumpQueue("PONG : " . $lineArray[1]);
			continue;
		}else if($command=="PONG"){
			$pingLagTime=round(microtime(true)*1000)-(int)$lineArray[1];
			cussbotLog(LOG_INFO,"Received PONG command, lag time was $pingLagTime ms.");
		}else if($command=="001"){
			cussbotLog(LOG_NOTICE,"Received 001 command, enabling PINGs.");
			$authenticated=true;
			$lastPingTime=$currentTime;
			$config->joinConfiguredChannels();
		}else if($command=="353"){
			$names=array();
			foreach(explode(" ",$lineArray[1]) as $name){
				while(in_array($name[0],$namePrefixes)){
					$name=substr($name,1);
				}
				$names[$name]=null;
			}
			$config->onNamesList($prefixArray[4],$names);
		}else if($command=="366"){
			$config->onEndNamesList($prefixArray[3]);
		}else if($command=="403"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="405"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="433"){
			if($authenticated){
				if($botNick!=$curBotNick){
					cussbotLog(LOG_WARNING,"Received 433 command while authenticated, attempting REGAIN…");
					nickServRegain();
				}
			}else{
				cussbotLog(LOG_WARNING,"Received 433 command, trying alternate nick…");
				$curBotNick=$curBotNick . "_";
				ircCommand("NICK " . $curBotNick);
			}
		}else if($command=="435"){
			cussbotLog(LOG_WARNING,"Received 435 command, parting channel \"$prefixArray[4]\" to regain primary nick…");
			$config->partChannels($prefixArray[4],"Parting channel due to primary nick ban in order to regain primary nick.");
			ircCommand("NICK " . $botNick);
			$config->joinChannels($prefixArray[4]);
		}else if($command=="437"){
			cussbotLog(LOG_WARNING,"Received 437 command, trying alternate nick and releasing primary nick…");
			$curBotNick=$curBotNick . "_";
			ircCommand("NICK " . $curBotNick);
			nickServRelease();
		}else if($command=="442"){
			$config->onNotInChannel($prefixArray[3]);
		}else if($command=="470"){
			$config->onJoinRedirect($prefixArray[3],$prefixArray[4],$command,$lineArray[1]);
		}else if($command=="471"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="473"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="474"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="475"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="476"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="477"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="479"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="480"){
			$config->onJoinFailure($prefixArray[3],$command,$lineArray[1]);
		}else if($command=="707"){
			cussbotLog(LOG_WARNING,"Received 707 command, queueing lastCommand on retransmitQueue");
			if($lastCommand!=null){
				array_push($retransmitQueue,$lastCommand);
			}
		}else if($command=="900"){
			$botHostname=explode("!",$prefixArray[3])[1];
		}else if($command=="903"){
			ircCommand("CAP END");
			if($botNick!=$curBotNick){
				nickServRegain();
			}
		}else if($command=="CAP"){
			if($lineArray[1]=="sasl"){
				ircCommand("AUTHENTICATE PLAIN");
				ircCommand("AUTHENTICATE " . base64_encode("$nickservAccount\0$nickservAccount\0$nickservPassword"));
			}
		}
	}
}

function str_startswith($str,$sub){
    return (mb_substr($str,0,mb_strlen($sub))==$sub);
}

function str_endswith($str,$sub){
    return (mb_substr($str,mb_strlen($str)-mb_strlen($sub))==$sub);
}

function ircConnect(){
	$GLOBALS["config"]->resetChannels();
	
	while(true){
		cussbotLog(LOG_NOTICE,"Attempting to connect to " . $GLOBALS["ircServers"][$GLOBALS["ircServerIndex"]]["server"] . "…");
		$GLOBALS["ircConnection"]=stream_socket_client($GLOBALS["ircServers"][$GLOBALS["ircServerIndex"]]["server"] . ":" . $GLOBALS["ircServers"][$GLOBALS["ircServerIndex"]]["port"]);
		if($GLOBALS["ircConnection"]===false){
			cussbotLog(LOG_WARNING,"Failed to connect to server, attempting next server…");
			$GLOBALS["ircServerIndex"]++;
			if($GLOBALS["ircServerIndex"]>=count($GLOBALS["ircServers"])){
				$GLOBALS["ircServerIndex"]=0;
			}
		}else{
			$GLOBALS["lastConnectionTime"]=microtime(true);
			stream_set_blocking($GLOBALS["ircConnection"],0);
			$metadata=stream_get_meta_data($GLOBALS["ircConnection"]);
			cussbotLog(LOG_NOTICE,"Successfully connected to server " . (array_key_exists("crypto",$metadata)?"using the " . $metadata["crypto"]["protocol"] . " protocol with the " . $metadata["crypto"]["cipher_bits"] . "-bit " . $metadata["crypto"]["cipher_name"] . " cipher":"without encryption") . ", beginning authentication…");
			break;
		}
	}
	
	ircCommand("CAP REQ :sasl");
	ircCommand("NICK " . $GLOBALS["curBotNick"]);
	ircCommand("USER " . $GLOBALS["curBotNick"] . " 8 * : " . $GLOBALS["botName"]);
}

function ircDisconnect(){
	cussbotLog(LOG_NOTICE,"Disconnecting from IRC…");
	fclose($GLOBALS["ircConnection"]);
	resetState();
}

function ircReconnect(){
	ircDisconnect();
	ircConnect();
}

function ircCommand($command){
	if($GLOBALS['authenticated']){
		$currentTime=microtime(true);
		foreach($GLOBALS["rateLimitData"] as $key=>$value){
			if($value<$currentTime-$GLOBALS["rateLimitInterval"]||$value>$currentTime){
				unset($GLOBALS["rateLimitData"][$key]);
			}
		}
		if(count($GLOBALS["rateLimitData"])>$GLOBALS["maxMessageBurst"]-1||count($GLOBALS["transmitQueue"])>0){
			cussbotLog(LOG_INFO,"Too many messages sent too quickly, throttling…");
			if(count($GLOBALS["transmitQueue"])==0){
				$GLOBALS["nextTimeoutTime"]=microtime(true)+$GLOBALS["rateLimitInterval"];
			}
			array_push($GLOBALS["transmitQueue"],$command);
		}else{
			ircCommandNoRateLimit($command);
		}
	}else{
		ircCommandNoRateLimit($command);
	}
}

function ircCommandJumpQueue($command){
	if($GLOBALS["authenticated"]&&count($GLOBALS["transmitQueue"])>0){
		array_unshift($GLOBALS["transmitQueue"],$command);
	}else{
		ircCommandNoRateLimit($command);
	}
}

function ircCommandNoRateLimit($command){
	cussbotLog(LOG_DEBUG,"IRC SEND: \"$command\"");
	$lastCommand=$command;
	array_push($GLOBALS["rateLimitData"],microtime(true));
	fwrite($GLOBALS["ircConnection"],"$command\n");
}

function ircPrivmsg($message,$target=null){
	ircMessage("PRIVMSG",$target,$message);
}

function ircNotice($message,$target=null){
	ircMessage("NOTICE",$target,$message);
}

function ircMessage($command,$target,$message){
	if($message!=""){
		if($target==null){
			$target=$GLOBALS["target"];
		}
		$curMaxCommandLength=$GLOBALS["maxCommandLength"]-strlen($command)-strlen($target)-strlen($GLOBALS["curBotNick"])-strlen($GLOBALS["botHostname"])-8;
		if($curMaxCommandLength<=0){
			cussbotLog(LOG_ERR,"Base command longer than maximum line length!  Cannot send message.");
			return;
		}
		while(strlen($message)>0){
			$cutPosition=$curMaxCommandLength;
			if(strlen($message)>$curMaxCommandLength){
				while($cutPosition>0&&!ctype_space(mb_substr(mb_strcut($message,$cutPosition,4),0,1))){
					$cutPosition--;
				}
				if($cutPosition==0){
					$cutPosition=$curMaxCommandLength;
				}
			}
			$curMessage=trim(mb_strcut($message,0,$cutPosition));
			$message=trim(mb_strcut($message,$cutPosition));
			ircCommand("$command $target :$curMessage");
		}
	}
}

function ircAuto($message,$address=true){
	if($address&&$GLOBALS["target"]!=$GLOBALS["sender"]){
		$message=$GLOBALS["sender"] . ": $message";
	}
	if($GLOBALS["command"]=="NOTICE"){
		ircNotice($message);
	}else{
		ircPrivmsg($message);
	}
}

function nickServRelease(){
	ircCommand("PRIVMSG NickServ :RELEASE " . $GLOBALS["botNick"]);
	ircCommand("PRIVMSG NickServ :RELEASE " . $GLOBALS["botNick"] . "_");
	ircCommand("PRIVMSG NickServ :RELEASE " . $GLOBALS["botNick"] . "__");
}

function nickServRegain(){
	cussbotLog(LOG_NOTICE,"Attempting to REGAIN primary bot nick…");
	ircCommand("PRIVMSG NickServ :REGAIN " . $GLOBALS["botNick"]);
}

function resetState(){
	$GLOBALS["curBotNick"]=$GLOBALS["botNick"];
	$GLOBALS["pongsMissed"]=0;
	$GLOBALS["authenticated"]=false;
	$GLOBALS["lastConnectionTime"]=0;
	$GLOBALS["nextTimeoutTime"]=microtime(true)+$GLOBALS["pingInterval"];
	$GLOBALS["pingLagTime"]=0;
	$GLOBALS["lastPingTime"]=0;
	$GLOBALS["botHostname"]="";
	$GLOBALS["incompleteLine"]=NULL;
	$GLOBALS["incompleteLinesReceived"]=0;
	$GLOBALS["transmitQueue"]=array();
	$GLOBALS["rateLimitData"]=array();
	$GLOBALS["retransmitQueue"]=array();
	$GLOBALS["lastCommand"]=null;
	$GLOBALS["memoServData"]=null;
}

function stripIrcFormatting($text){
    return preg_replace("/(\x02|\x0F|\x1D|\x1F|\x03[0-9]{1,2}(,[0-9]{1,2})?|\x03)/","",$text);
}

function removeDupeSpaces($text){
	return preg_replace("!\s+!"," ",$text);
}

function wrapCtcp($string){
	return "\001$string\001";
}

function cussbotLog($priority,$message){
	if($GLOBALS["logVerbose"]||$priority<=LOG_NOTICE){
		if($GLOBALS["useSyslog"]){
			syslog($priority,$message);
		}else{
			print(date("Ymd H:i:s") . ": " . $GLOBALS["syslogLevels"][$priority] . ": " . $message . "\n");
		}
	}
}
?>
