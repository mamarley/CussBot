<?php
class CussWordStorage{
	private $database;
	
	private $sqlHostname;
	private $sqlUsername;
	private $sqlPassword;
	private $sqlDatabaseName;
	
	private $pingStatement;
	
	private $selectWordsStatement;
	private $insertWordStatement;
	private $deleteWordStatement;
	
	private $selectCategoriesStatement;
	private $insertCategoryStatement;
	private $deleteCategoryStatement;
	
	private $selectWhitelistwordsStatement;
	private $insertWhitelistwordStatement;
	private $deleteWhitelistwordStatement;
	private $deleteUsesByWhitelistwordStatement;
	
	private $selectNickIdStatement;
	private $insertNickStatement;
	private $selectChannelIdStatement;
	private $insertChannelStatement;
	private $insertUseStatement;
	
	private $cussWords=array();
	private $whitelistWords=array();
	
	public function __construct($sqlHostname,$sqlPort,$sqlUsername,$sqlPassword,$sqlDatabaseName){
		$this->sqlHostname=$sqlHostname;
		$this->sqlPort=$sqlPort;
		$this->sqlUsername=$sqlUsername;
		$this->sqlPassword=$sqlPassword;
		$this->sqlDatabaseName=$sqlDatabaseName;
		
		$this->sqlConnect();
	}
	
	private function sqlConnect(){
		try{
			$this->database=null;
			
			cussbotLog(LOG_NOTICE,"Attempting to connect to database server…");
			$this->database=new PDO("pgsql:host=" . $this->sqlHostname . " port=" . $this->sqlPort . " dbname=" . $this->sqlDatabaseName,$this->sqlUsername,$this->sqlPassword);
			
			$this->pingStatement=$this->database->prepare("SELECT 1");
			
			$this->selectWordsStatement=$this->database->prepare("SELECT words.wordid,words.word,categories.categoryid,categories.shortname FROM words JOIN categories ON words.categoryid=categories.categoryid");
			$this->insertWordStatement=$this->database->prepare("INSERT INTO words (categoryid, word) VALUES ((SELECT categoryid FROM categories WHERE shortname=:shortname),:word) RETURNING wordid");
			$this->deleteWordStatement=$this->database->prepare("DELETE FROM words WHERE word=:word");
			
			$this->selectCategoriesStatement=$this->database->prepare("SELECT shortname FROM categories");
			$this->insertCategoryStatement=$this->database->prepare("INSERT INTO categories (shortname, fullname) VALUES (:shortname,:fullname)");
			$this->deleteCategoryStatement=$this->database->prepare("DELETE FROM categories WHERE shortname=:shortname");
			
			$this->selectWhitelistwordsStatement=$this->database->prepare("SELECT whitelistword FROM whitelistwords");
			$this->insertWhitelistwordStatement=$this->database->prepare("INSERT INTO whitelistwords (whitelistword) VALUES (:whitelistword)");
			$this->deleteWhitelistwordStatement=$this->database->prepare("DELETE FROM whitelistwords WHERE whitelistword=:whitelistword");
			$this->deleteUsesByWhitelistwordStatement=$this->database->prepare("DELETE FROM uses WHERE lower(word) LIKE :whitelistword");
			
			$this->selectNickIdStatement=$this->database->prepare("SELECT nickid FROM nicks WHERE nick=:nick");
			$this->insertNickStatement=$this->database->prepare("INSERT INTO nicks (nick) VALUES (:nick) RETURNING nickid");
			$this->selectChannelIdStatement=$this->database->prepare("SELECT channelid FROM channels WHERE channel=:channel");
			$this->insertChannelStatement=$this->database->prepare("INSERT INTO channels (channel) VALUES (:channel) RETURNING channelid");
			$this->insertUseStatement=$this->database->prepare("INSERT INTO uses (nickid,channelid,wordid,word) VALUES (:nickid,:channelid,:wordid,:word)");
			
			$this->loadCussWords();
			$this->loadWhitelistWords();
		}catch(Exception $e){
			cussbotLog(LOG_ERR,"Failed to connect to database server with message \"" . $e->getMessage() . "\"!");
		}
		cussbotLog(LOG_NOTICE,"Successfully connected to database server.");
	}
	
	public function ping(){
		if(!$this->pingWithoutReconnect()){
			$this->sqlConnect();
		}
	}
	
	private function pingWithoutReconnect(){
		if($this->database==null){
			cussbotLog(LOG_WARNING,"Failed to ping database server because connection is null.");
			return false;
		}
		
		try{
			$this->errorHandleExec($this->pingStatement);
		}catch(Exception $e){
			cussbotLog(LOG_WARNING,"Failed to ping database server with message \"" . $e->getMessage() . "\".");
			return false;
		}
		
		return true;
	}

	public function loadCussWords(){
		$this->pingWithoutReconnect();
		
		cussbotLog(LOG_INFO,"(re)Loading cuss words from database.");
		$this->errorHandleExec($this->selectWordsStatement);
		$this->cussWords=$this->selectWordsStatement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function addWord($categoryShortName,$word){
		$this->ping();
		
		$lowerCategoryShortName=mb_strtolower($categoryShortName);
		$lowerWord=mb_strtolower($word);
		
		$this->insertWordStatement->bindParam(":shortname",$lowerCategoryShortName);
		$this->insertWordStatement->bindParam(":word",$lowerWord);
		$this->errorHandleExec($this->insertWordStatement);
		
		if($this->insertWordStatement->rowCount()==1){
			$cussWord=array();
			$cussWord["wordid"]=$this->insertWordStatement->fetch(PDO::FETCH_ASSOC)["wordid"];
			$cussWord["word"]=$lowerWord;
			array_push($this->cussWords,$cussWord);
			return true;
		}else{
			return false;
		}
	}
	
	public function removeWord($word){
		$this->ping();
		
		$lowerWord=mb_strtolower($word);
		
		$this->deleteWordStatement->bindParam(":word",$lowerWord);
		$this->errorHandleExec($this->deleteWordStatement);
		
		if($this->deleteWordStatement->rowCount()==1){
			foreach($this->cussWords as $key=>$value){
				if($value["word"]==$lowerWord){
					unset($this->cussWords[$key]);
				}
			}
			return true;
		}else{
			return false;
		}
	}
	
	public function getCategories(){
		$this->ping();
		
		$this->errorHandleExec($this->selectCategoriesStatement);
		return $this->selectCategoriesStatement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function addCategory($shortName,$fullName){
		$this->ping();
		
		$lowerShortName=mb_strtolower($shortName);
		
		$this->insertCategoryStatement->bindParam(":shortname",$lowerShortName);
		$this->insertCategoryStatement->bindParam(":fullname",$fullName);
		$this->errorHandleExec($this->insertCategoryStatement);
		
		if($this->insertCategoryStatement->rowCount()==1){
			return true;
		}else{
			return false;
		}
	}
	
	public function removeCategory($shortName){
		$this->ping();
		
		$lowerShortName=mb_strtolower($shortName);
		
		$this->deleteCategoryStatement->bindParam(":shortname",$lowerShortName);
		$this->errorHandleExec($this->deleteCategoryStatement);
		
		if($this->deleteCategoryStatement->rowCount()==1){
			$this->loadCussWords();
			return true;
		}else{
			return false;
		}
	}
	
	public function loadWhitelistWords(){
		$this->pingWithoutReconnect();
		
		cussbotLog(LOG_INFO,"(re)Loading whitelist words from database.");
		$this->errorHandleExec($this->selectWhitelistwordsStatement);
		$this->whitelistWords=$this->selectWhitelistwordsStatement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	public function getWhitelistWords(){
		return $this->whitelistWords;
	}
	
	public function addWhitelistWord($whitelistWord){
		$this->ping();
		
		$lowerWhitelistWord=mb_strtolower($whitelistWord);
		$deleteUsesParameter="%" . mb_strtolower($whitelistWord) . "%";
		
		$this->insertWhitelistwordStatement->bindParam(":whitelistword",$lowerWhitelistWord);
		$this->errorHandleExec($this->insertWhitelistwordStatement);
		
		if($this->insertWhitelistwordStatement->rowCount()==1){
			$whitelistWord=array();
			$whitelistWord["whitelistword"]=$lowerWhitelistWord;
			array_push($this->whitelistWords,$whitelistWord);
			
			$this->deleteUsesByWhitelistwordStatement->bindParam(":whitelistword",$deleteUsesParameter);
			$this->errorHandleExec($this->deleteUsesByWhitelistwordStatement);
			return true;
		}else{
			return false;
		}
	}
	
	public function removeWhitelistWord($whitelistWord){
		$this->ping();
		
		$lowerWhitelistWord=mb_strtolower($whitelistWord);
		
		$this->deleteWhitelistwordStatement->bindParam(":whitelistword",$lowerWhitelistWord);
		$this->errorHandleExec($this->deleteWhitelistwordStatement);
		
		if($this->deleteWhitelistwordStatement->rowCount()==1){
			foreach($this->whitelistWords as $key=>$value){
				if($value["whitelistword"]==$lowerWhitelistWord){
					unset($this->whitelistWords[$key]);
				}
			}
			return true;
		}else{
			return false;
		}
	}
	
	public function onMessage($command,$sender,$target,$message){
		$strippedNick=self::stripNonAlphanumeric($sender);
		if(mb_strlen($strippedNick)==0){
			$strippedNick=$sender;
		}
		
		$nickId=null;
		$channelId=null;
		
		$messageWords=explode(" ",$message);
		foreach($messageWords as $origMessageWord){
			$messageWord=mb_strtolower($origMessageWord);
			
			foreach($this->whitelistWords as $whitelistWord){
				$messageWord=str_replace($whitelistWord["whitelistword"],"",$messageWord);
			}
			
			foreach($this->cussWords as $cussWord){
				$cussCount=mb_substr_count($messageWord,$cussWord["word"]);
				
				if($cussCount>0){
					if($nickId===null){
						$this->selectNickIdStatement->bindParam(":nick",$strippedNick);
						$this->errorHandleExec($this->selectNickIdStatement);
						if($this->selectNickIdStatement->rowCount()==1){
							$nickId=$this->selectNickIdStatement->fetch(PDO::FETCH_ASSOC)["nickid"];
						}else{
							$this->insertNickStatement->bindParam(":nick",$strippedNick);
							$this->errorHandleExec($this->insertNickStatement);
							$nickId=$this->insertNickStatement->fetch(PDO::FETCH_ASSOC)["nickid"];
						}
					}
					
					if($channelId===null){
						$this->selectChannelIdStatement->bindParam(":channel",$target);
						$this->errorHandleExec($this->selectChannelIdStatement);
						if($this->selectChannelIdStatement->rowCount()==1){
							$channelId=$this->selectChannelIdStatement->fetch(PDO::FETCH_ASSOC)["channelid"];
						}else{
							$this->insertChannelStatement->bindParam(":channel",$target);
							$this->errorHandleExec($this->insertChannelStatement);
							$channelId=$this->insertChannelStatement->fetch(PDO::FETCH_ASSOC)["channelid"];
						}
					}
				}
				
				for($i=0;$i<$cussCount;$i++){
					$this->insertUseStatement->bindParam(":nickid",$nickId);
					$this->insertUseStatement->bindParam(":channelid",$channelId);
					$this->insertUseStatement->bindParam(":wordid",$cussWord["wordid"]);
					$this->insertUseStatement->bindParam(":word",$origMessageWord);
					$this->errorHandleExec($this->insertUseStatement);
				}
			}
		}
	}
	
	public function getStats($statsType,$channelNotInArgs,$nickNotInArgs,$channelInArgs,$nickInArgs,$limit){
		$this->ping();
		
		$channelNotInPlaceholders=array_fill(0,count($channelNotInArgs),"?");
		$nickNotInPlaceholders=array_fill(0,count($nickNotInArgs),"?");
		$channelInPlaceholders=array_fill(0,count($channelInArgs),"?");
		$nickInPlaceholders=array_fill(0,count($nickInArgs),"?");
		
		if(count($channelNotInArgs)>0){
			$channelNotInString="lower(channel) NOT IN (" . implode(",",$channelNotInPlaceholders) . ")";
		}else{
			$channelNotInString="true";
		}
		
		if(count($nickNotInArgs)>0){
			$nickNotInString="lower(nick) NOT IN (" . implode(",",$nickNotInPlaceholders) . ")";
		}else{
			$nickNotInString="true";
		}
		
		if(count($channelInArgs)>0){
			$channelInString="lower(channel) IN (" . implode(",",$channelInPlaceholders) . ")";
		}else{
			$channelInString="true";
		}
		
		if(count($nickInArgs)>0){
			$nickInString="lower(nick) IN (" . implode(",",$nickInPlaceholders) . ")";
		}else{
			$nickInString="true";
		}
		
		if($statsType=="categorystats"){
			$statsStatement=$this->database->prepare("SELECT fullname, COUNT(fullname) AS count FROM uses JOIN words ON uses.wordid=words.wordid JOIN categories ON words.categoryid=categories.categoryid " . (count($nickNotInArgs)>0||count($nickInArgs)>0?"JOIN nicks ON uses.nickid=nicks.nickid ":"") . (count($channelNotInArgs)>0||count($channelInArgs)>0?"JOIN channels ON uses.channelid=channels.channelid ":"") . "WHERE $channelNotInString AND $nickNotInString AND $channelInString AND $nickInString GROUP BY fullname LIMIT ?");
		}else if($statsType=="channelstats"){
			$statsStatement=$this->database->prepare("SELECT channel, COUNT(channel) AS count FROM uses  " . (count($nickNotInArgs)>0||count($nickInArgs)>0?"JOIN nicks ON uses.nickid=nicks.nickid ":"") . "JOIN channels ON uses.channelid=channels.channelid WHERE $channelNotInString AND $nickNotInString AND $channelInString AND $nickInString GROUP BY channel ORDER BY count DESC LIMIT ?");
		}else if($statsType=="nickstats"){
			$statsStatement=$this->database->prepare("SELECT nick, COUNT(nick) AS count FROM uses JOIN nicks ON uses.nickid=nicks.nickid " . (count($channelNotInArgs)>0||count($channelInArgs)>0?"JOIN channels ON uses.channelid=channels.channelid ":"") . "WHERE $channelNotInString AND $nickNotInString AND $channelInString AND $nickInString GROUP BY nick ORDER BY count DESC LIMIT ?");
		}else if($statsType=="wordstats"){
			$statsStatement=$this->database->prepare("SELECT words.word AS word,COUNT(words.wordid) AS count FROM uses JOIN words ON uses.wordid=words.wordid " . (count($nickNotInArgs)>0||count($nickInArgs)>0?"JOIN nicks ON uses.nickid=nicks.nickid ":"") . (count($channelNotInArgs)>0||count($channelInArgs)>0?"JOIN channels ON uses.channelid=channels.channelid ":"") . "WHERE $channelNotInString AND $nickNotInString AND $channelInString AND $nickInString GROUP BY words.wordid ORDER BY count DESC LIMIT ?");
		}else{
			throw new Exception("Invalid statistics type \"$statsType\".");
		}
		
		$i=0;
		foreach($channelNotInArgs as $channelNotInArg){
			$statsStatement->bindValue(++$i,mb_strtolower($channelNotInArg));
		}
		foreach($nickNotInArgs as $nickNotInArg){
			$strippedNickNotInArg=self::stripNonAlphanumeric($nickNotInArg);
			if(mb_strlen($strippedNickNotInArg)==0){
				$strippedNickNotInArg=$nickNotInArg;
			}
			$statsStatement->bindValue(++$i,mb_strtolower(self::stripNonAlphanumeric($strippedNickNotInArg)));
		}
		foreach($channelInArgs as $channelInArg){
			$statsStatement->bindValue(++$i,mb_strtolower($channelInArg));
		}
		foreach($nickInArgs as $nickInArg){
			$strippedNickInArg=self::stripNonAlphanumeric($nickInArg);
			if(mb_strlen($strippedNickInArg)==0){
				$strippedNickInArg=$nickInArg;
			}
			$statsStatement->bindValue(++$i,mb_strtolower(self::stripNonAlphanumeric($strippedNickInArg)));
		}
		$statsStatement->bindValue(++$i,$limit);
		$this->errorHandleExec($statsStatement);
		return $statsStatement->fetchAll(PDO::FETCH_ASSOC);
	}
	
	private function errorHandleExec($statement){
		$statement->execute();
		$errorInfo=$statement->errorInfo();
		if($errorInfo[0]!="00000"){
			throw new Exception("SQL query failed with SQLSTATE $errorInfo[0]: \"$errorInfo[2]\" ($errorInfo[1]) .");
		}
	}
	
	private static function stripNonAlphanumeric($text){
		return preg_replace("/[^a-zA-Z0-9\s]/","",$text);
	}
}
?>
